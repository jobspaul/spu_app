//
//  FacultyCollectionViewCell.swift
//  spu
//
//  Created by Nattawut Nokyoo on 6/26/17.
//  Copyright © 2017 spu. All rights reserved.
//

import UIKit

class FacultyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
}
