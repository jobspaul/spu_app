//
//  FacultyTableViewCell.swift
//  spu
//
//  Created by Nattawut Nokyoo on 6/28/17.
//  Copyright © 2017 spu. All rights reserved.
//

import UIKit

class FacultyTableViewCell: UITableViewCell {
    @IBOutlet weak var showImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
