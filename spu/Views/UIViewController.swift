//
//  UIViewController.swift
//  ab-money
//
//  Created by Sutham on 9/13/2560 BE.
//  Copyright © 2560 komsit. All rights reserved.
//

import UIKit
import CoreLocation
//import MBProgressHUD

fileprivate let swizzling: (UIViewController.Type) -> () = { viewController in
    let originalSelector = #selector(viewController.viewWillAppear(_:))
    let swizzledSelector = #selector(viewController.proj_viewWillAppear(animated:))
    
    let originalMethod = class_getInstanceMethod(viewController, originalSelector)
    let swizzledMethod = class_getInstanceMethod(viewController, swizzledSelector)
    
    let didAddMethod = class_addMethod(viewController, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
    
    if didAddMethod {
        class_replaceMethod(viewController, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
}

extension UIViewController {
    
    open override class func initialize() {
        // make sure this isn't a subclass
        guard self === UIViewController.self else { return }
        swizzling(self)
    }
    
    // Mark: - AddChildView
    func addChildView(vc: UIViewController, mainVC: UIView, frame: CGRect) {
        addChildViewController(vc)
        vc.view.frame = frame
        mainVC.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
    // Mark: - RemoveChildView
    func removeChildView(vc: UIViewController) {
        vc.willMove(toParentViewController: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParentViewController()
    }
    
    // MARK: - Method Swizzling
    func proj_viewWillAppear(animated: Bool) {
        self.proj_viewWillAppear(animated: animated)
        
        // customize back button
        if customizeBackButton() {
            /*
             if self.navigationController is MainNavigationController {
             for vc in (navigationController?.viewControllers)! {
             if vc is TermAgreementViewController {
             return
             }
             }
             */
            setupBackButton()
        }
    }
    
    func setupBackButton() {
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Back"),
                                         style: .plain,
                                         target: self, action: #selector(self.back))
        backButton.tintColor = UIColor.black
        self.navigationItem.setLeftBarButton(backButton, animated: true)
    }
    
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // override this func if you do not want back button to be customized in a specific view controller
    func customizeBackButton() -> Bool {
        return true
    }
    
    // MARK: - Base methods
    func alert(message: String!) {
        alert(message: message, title: nil, buttonTitle: nil, callback: nil)
        
    }
    
    func alert(message: String!, callback: @escaping() -> ()) {
        alert(message: message, title: nil, buttonTitle: nil, callback: callback)
    }
    
    func alert(message: String!, title: String!) {
        alert(message: message, title: title, buttonTitle: nil, callback: nil)
    }
    
    func alert(message: String!, title: String!, callback: @escaping() -> ()) {
        alert(message: message, title: title, buttonTitle: nil, callback: callback)
    }
    
    func alert(message: String!, title: String?, buttonTitle: String?, callback: (() -> ())?) {
        var _buttonTitle = "Ok"
        if let _ = buttonTitle {
            _buttonTitle = buttonTitle!
        }
        alert(message: message, title: title, leftButtonTitle: _buttonTitle, leftButtonCallback: callback, rightButtonTitle: nil, rightButtonCallback: nil)
    }
    
    func alert(message: String!, title: String?, leftButtonTitle: String!, leftButtonCallback: (() -> ())?, rightButtonTitle: String?, rightButtonCallback: (() -> ())?) {
        let alertController = UIAlertController(title: title ?? "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        
        let leftAction = UIAlertAction(title: leftButtonTitle, style: UIAlertActionStyle.default) { result in
            leftButtonCallback?()
        }
        alertController.addAction(leftAction)
        
        if let _ = rightButtonTitle {
            let rightAction = UIAlertAction(title: rightButtonTitle!, style: UIAlertActionStyle.default) { result in
                rightButtonCallback?()
            }
            alertController.addAction(rightAction)
        }
        
        // show alert dialog
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*func showHUDProgress(withMessage message: String?) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideHUDProgress() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }*/
}
