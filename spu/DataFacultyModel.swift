//
//  DataFacultyModel.swift
//  spu
//
//  Created by Nattawut Nokyoo on 7/11/18.
//  Copyright © 2018 spu. All rights reserved.
//

import Foundation
import UIKit

class DataFacultyModel: NSObject {
    var dataFaculty = ["Sripatum International College",
                       "วิทยาลัยท่องและการบริการ",
                       "คณะศิลปศาสตร์",
                       "คณะนิเทศศาสตร์",
                       "คณะบริหารธุรกิจ",
                       "วิทยาลัยโลจิสติกส์และซัพพลายเชน",
                       "คณะบัญชี",
                       "คณะวิศวกรรม",
                       "คณะเทคโนโลยีสารสนเทศ",
                       "คณะสถาปัตยกรรมศาสตร์",
                       "คณะดิจิทัลมีเดีย",
                       "คณะนิติศาสตร์"]
    
    var imageArr: NSArray = [UIImage(named: "IconSripatumInternational")!,
    UIImage(named: "Iconวิทยาลัยการท่องเที่ยว")!,
    UIImage(named: "Iconคณะศิลปศาสตร์")!,
    UIImage(named: "Iconคณะนิเทศศาสตร์")!,
    UIImage(named: "Iconคณะบริหารธุรกิจ")!,
    UIImage(named: "Iconวิทยาลัยโลจิสติกส์")!,
    UIImage(named: "Iconคณะบัญชี")!,
    UIImage(named: "Iconคณะวิศวกรรมศาสตร์")!,
    UIImage(named: "Iconคณะเทคโนโลยีสารสนเทศ")!,
    UIImage(named: "Iconคณะสถาปัตยกรรมศาสตร์")!,
    UIImage(named: "Iconคณะดิจิทัลมีเดีย")!,
    UIImage(named: "Iconคณะนิติศาสตร์")!]
    
    var imageArrMap: NSArray  = [UIImage(named: "BGวิทยาลัยนานาชาติ")!,
    UIImage(named: "BGวิทยาลัยการท่องเที่ยวและการบริการ")!,
    UIImage(named: "BGคณะศิลปศาสตร์")!,
    UIImage(named: "BGคณะนิเทศศาสตร์")!,
    UIImage(named: "BGคณะบริหารธุรกิจ")!,
    UIImage(named: "BGคณะโลจิสติกส์และซัพพลายเชน")!,
    UIImage(named: "BGคณะบัญชี")!,
    UIImage(named: "BGคณะวิศวกรรมศาสตร์")!,
    UIImage(named: "BGคณะเทคโนโลยีสารสนเทศ")!,
    UIImage(named: "BGคณะสถาปัตยกรรมศาสตร์")!,
    UIImage(named: "BGคณะดิจิทัลมีเดีย")!,
    UIImage(named: "BGคณะนิติศาสตร์")!]
    
    var dataOffices = ["สำนักงานทะเบียน",
                       "สำนักงานการคลัง",
                       "สำนักงานวิชาการ",
                       "ห้องบัวหลวง",
                       "สำนักงานอาคารและสถานที่",
                       "กลุ่มงานกิจกรรมการนักศึกษา"]
}
