//
//  MapSpuTableViewController.swift
//  spu
//
//  Created by Nattawut Nokyoo on 9/19/17.
//  Copyright © 2017 spu. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapSpuTableViewController: UITableViewController {

    @IBOutlet weak var mapSpu: MKMapView!
    
     var locationArr: NSArray = []
    
    
    func setMapView() {
        let location9 = CLLocationCoordinate2DMake(13.854234,100.585897)
        let location5 = CLLocationCoordinate2DMake(13.854006, 100.585302)
        let location10 = CLLocationCoordinate2DMake(13.854669, 100.585923)
        let location2 = CLLocationCoordinate2DMake(13.854822, 100.584749)
        let location1 = CLLocationCoordinate2DMake(13.855356, 100.585165)
        let location11 = CLLocationCoordinate2DMake(13.855479, 100.585712)
        let location6 = CLLocationCoordinate2DMake(13.853647, 100.586048)
        let locationFood = CLLocationCoordinate2DMake(13.853496, 100.585301)
        let location4 = CLLocationCoordinate2DMake(13.853725, 100.584930)
        let locationVersatile = CLLocationCoordinate2DMake(13.853503, 100.584794)
        
        let span = MKCoordinateSpanMake(0.002, 0.002)
        let region = MKCoordinateRegionMake(location9, span)
        //let region2 = MKCoordinateSpanMake(locationTwo, span)
        
        mapSpu.setRegion(region, animated: true)
        //mapSpu.setRegion(region2, animated: true)
        
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location9
        annotation.title = "ตึก 9"
        annotation.subtitle = "Luna Rossa"
        mapSpu.addAnnotation(annotation)
        
        //Showing the device location on the map
        
        
        let annotation2 = MKPointAnnotation()
        annotation2.coordinate = location5
        annotation2.title = "ตึก 5"
        annotation2.subtitle = "Luna Rossa"
        mapSpu.addAnnotation(annotation2)
        
        
        let annotation3 = MKPointAnnotation()
        annotation3.coordinate = location10
        annotation3.title = "ตึก 10"
        annotation3.subtitle = "โรงอาหาร"
        mapSpu.addAnnotation(annotation3)
        
        let annotation4 = MKPointAnnotation()
        annotation4.coordinate = location2
        annotation4.title = "ตึก 2"
        annotation4.subtitle = "Luna Rossa"
        mapSpu.addAnnotation(annotation4)
        
        let annotation5 = MKPointAnnotation()
        annotation5.coordinate = location1
        annotation5.title = "ตึก 1"
        annotation5.subtitle = "Luna Rossa"
        mapSpu.addAnnotation(annotation5)
        
        let annotation6 = MKPointAnnotation()
        annotation6.coordinate = location11
        annotation6.title = "ตึก 11"
        annotation6.subtitle = "Luna Rossa"
        mapSpu.addAnnotation(annotation6)
        
        let annotation7 = MKPointAnnotation()
        annotation7.coordinate = location6
        annotation7.title = "ตึก 6"
        annotation7.subtitle = "Luna Rossa"
        mapSpu.addAnnotation(annotation7)
        
        let annotation8 = MKPointAnnotation()
        annotation8.coordinate = locationFood
        annotation8.title = "โรงอาหาร"
        annotation8.subtitle = "Luna Rossa"
        mapSpu.addAnnotation(annotation8)
        
        let annotation9 = MKPointAnnotation()
        annotation9.coordinate = location4
        annotation9.title = "ตึก 4"
        annotation9.subtitle = "Luna Rossa"
        mapSpu.addAnnotation(annotation9)
        
        let annotation10 = MKPointAnnotation()
        annotation10.coordinate = locationVersatile
        annotation10.title = "อาคารเอนกประสงค์"
        annotation10.subtitle = "Luna Rossa"
        mapSpu.addAnnotation(annotation9)

        
        self.mapSpu.showsUserLocation = true;
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMapView()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

}

extension MapSpuTableViewController : MKMapViewDelegate {
   
    
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            guard !(annotation is MKUserLocation) else {
                return nil
            }
            
            let annotationIdentifier = "Identifier"
            var annotationView: MKAnnotationView?
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
                annotationView = dequeuedAnnotationView
                annotationView?.annotation = annotation
            } else {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
                annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            
            if let annotationView = annotationView {
                annotationView.canShowCallout = true
                annotationView.image = #imageLiteral(resourceName: "Pin")
                
            }
            return annotationView
        }
        
        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
            //self.callAppleMap()
        }
        
        func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
            
            if control == view.rightCalloutAccessoryView {
                self.callAppleMap()
            }
        }
        
        func callAppleMap() {
            self.alert(message: "ท่านต้องการเปิดนำทางหรือไม่", title: "", leftButtonTitle: "ต้องการ", leftButtonCallback: {
                UIApplication.shared.openURL(URL(string: "http://maps.apple.com/?ll=\(13.854234),\(100.585897)")!)
            
            }, rightButtonTitle: "ยกเลิก") {}
        }
        
}
