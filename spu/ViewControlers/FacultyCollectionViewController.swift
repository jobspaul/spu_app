//
//  FacultyCollectionViewController.swift
//  spu
//
//  Created by Nattawut Nokyoo on 6/26/17.
//  Copyright © 2017 spu. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class FacultyCollectionViewController: UICollectionViewController {
   
    var data: [DataModel] = []
    var dataSelect: DataModel?
    var selectData: DataModel?
    var dataMain = DataFacultyModel()
    
    func setdata() {
        for i in 0...dataMain.dataFaculty.count - 1 {
            let item = DataModel()
            item.name = dataMain.dataFaculty[i]
            item.dataImage = dataMain.imageArr[i] as? UIImage
            item.dataImageMap = dataMain.imageArrMap[i] as? UIImage
            data.append(item)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setdata()
        self.navigationItem.title = "ที่ตั้งคณะ"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return data.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FacultyCollectionViewCell
        let item = self.data[indexPath.row]
        cell.titleLabel.text = item.name
        cell.image.image = item.dataImage
        // Configure the cell
        return cell
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //debugPrint("didSlectAction")
        self.selectData = self.data[indexPath.row]
        self.performSegue(withIdentifier: "FacultyCollectionToDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FacultyCollectionToDetail" {
            if let vc = segue.destination as? DetailRegisterViewController {
                vc.dataPass = self.selectData
            }
        }
    }
}
