//
//  HomeViewController.swift
//  spu
//
//  Created by Komsit Developer on 6/25/2560 BE.
//  Copyright © 2560 spu. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Main Menu"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func regisAction(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "HomeToTab", sender: self)
    }

    @IBAction func facultyAction(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "HomeToFacultyCollection", sender: self)
    }
    
}

extension HomeViewController {
    //UINavigationBar Hide back Button Text
    override func setupBackButton() {
        let customBackButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = customBackButton
    }
}
