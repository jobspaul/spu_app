//
//  DetailRegisterViewController.swift
//  spu
//
//  Created by Komsit Developer on 6/25/2560 BE.
//  Copyright © 2560 spu. All rights reserved.
//

import UIKit
import SDWebImage

class DetailRegisterViewController: UIViewController {
    @IBOutlet weak var imageShow: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    var dataPass: DataModel?

    func setViews() {
        detailLabel.text = dataPass?.name
        self.imageShow.image = dataPass?.dataImageMap
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViews()
        self.navigationItem.title = dataPass?.name
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
