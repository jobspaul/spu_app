//
//  TabViewController.swift
//  spu
//
//  Created by Nattawut Nokyoo on 7/5/17.
//  Copyright © 2017 spu. All rights reserved.
//

import UIKit

class TabViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tab: UISegmentedControl!
   
    var dataMajor: [DataModel] = []
    var dataOffice: [DataModel] = []
    var dataCurrent: [DataModel] = []
    var dataSelect: DataModel?
    var selectData: DataModel?
    var dataMain = DataFacultyModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.dataCurrent = self.dataMajor
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.reloadData()
        self.navigationItem.title = "จุดลงทะเบียน"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData() {
        for i in 0...dataMain.dataFaculty.count - 1 {
            let item = DataModel()
            item.name = dataMain.dataFaculty[i]
            item.id = i
            item.dataImage = dataMain.imageArr[i] as? UIImage
            item.dataImageMap = dataMain.imageArrMap[i] as? UIImage
            dataMajor.append(item)
            
        }
        
        for i in 0...dataMain.dataOffices.count - 1 {
            let item = DataModel()
            item.name = dataMain.dataOffices[i]
            item.id = i
            dataOffice.append(item)
            
        }
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        if tab.selectedSegmentIndex == 0 {
            //showTab.text = "Hello"
            self.dataCurrent = self.dataMajor
        }
        
        if tab.selectedSegmentIndex == 1 {
            //showTab.text = "OneHello"
            self.dataCurrent = self.dataOffice
        }
        
        self.tableView.reloadData()
    }
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.dataCurrent.count
        //return 2
    }
    
    //เพิ่มขนาด List
    /*
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 100
     }
     */
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let item = self.dataCurrent[indexPath.row]
        
        // Configure the cell...
        cell?.textLabel?.text = item.name
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        dataSelect = self.dataCurrent[indexPath.row]
        
        self.performSegue(withIdentifier: "TabToDetail", sender: self)
        //self.performSegue(withIdentifier: "FacultyToDetailFaculty", sender: dataImage[indexPath.row])
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TabToDetail" {
            if let vc = segue.destination as? DetailFacultyTabViewController {
                vc.dataPass = self.dataSelect
            }
        }
    }
}


