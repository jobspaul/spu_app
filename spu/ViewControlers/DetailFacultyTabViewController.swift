//
//  DetailFacultyTabViewController.swift
//  spu
//
//  Created by Nattawut Nokyoo on 7/6/17.
//  Copyright © 2017 spu. All rights reserved.
//

import UIKit

class DetailFacultyTabViewController: UIViewController {
    @IBOutlet weak var showImage: UIImageView!
    @IBOutlet weak var showLabel: UILabel!
    var dataPass: DataModel?
    
    func setViews() {
        showLabel.text = dataPass?.name
        //self.showImage.sd_setImage(with: URL(string: (dataPass?.imageUrl)!), placeholderImage: #imageLiteral(resourceName: "Test"))
        showImage.image = dataPass?.dataImageMap
       
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViews()
        self.navigationItem.title = dataPass?.name
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
